const express = require('express');
const path = require('path');
const app = express();
const mainConfig = require('./config/app-config.json');
app.use(express.static('dist'));
app.use('/public', express.static('public'));
const session = require('express-session');
const Mongo = require('./modules/mongo.module');
const TokenService = require('./modules/token.module');
const ElasticSearch = require('./modules/elastic.module');
const elasticSearch = new ElasticSearch();
app.use(session({
    secret: 'sekret pass',
}));
app.listen(mainConfig.app.port, function () {
    console.log('Express server listening on port ' + mainConfig.app.port);
});
//elastic start
app.post('/api/suggest', function (req, res) {
    var data = '';
    req.on('data', function (chunk) {
        data += chunk.toString('utf8');
    });
    req.on('end', function () {
        elasticSearch.suggest(data, (result) => {
            res.json(JSON.stringify(result));
        }, (err) => {
            res.status(500).send('Search Engine Error.Sorry:(');
        });
    });

});

app.post('/api/search', function (req, res) {
    var data = [];
    req.on('data', function (chunk) {
        data.push(chunk);
    });
    req.on('end', function () {
        elasticSearch.search(data, (result) => {
            res.json(JSON.stringify(result));
        }, (err) => {
            res.status(500).send('An internal error occurred while performing the search request. Try later. I\'m sorry:(');
        });
    });
});
//elastic end
//auth start
const tokenServ = new TokenService("private"); //key for token encryption
app.post('/api/login', function (req, res) {
    var data = [];
    req.on('data', function (chunk) {
        data.push(chunk);
    });
    req.on('end', function () {
        data = Buffer
            .concat(data)
            .toString();
        const obj = JSON.parse(data);
        const result = equalsLogin(obj);
        req.session.token = result;
        res.json(JSON.stringify(result));

    });
});
app.post('/api/logout', function (req, res) {
    delete req.session.token;
    res.redirect(201, '/');
});
app.get('/api/token', function (req, res) {
    if (req.query.token) {
        const decToken = tokenServ.verifyToken(req.query.token);
        if (decToken) {
            req.session.token = req.query.token;
            res.json(JSON.stringify(req.session.token));
        } else {
            delete req.session.token;
            res.sendStatus(401);

        }
    }
});

function equalsLogin(obj) {
    //test config user
    const users = [{
        id: 1,
        login: "test",
        pass: "test",
        role: 1
    }, {
        id: 2,
        login: "test2",
        pass: "test2",
        role: 1
    }];
    for (var i = 0; i < users.length; i++) {
        var user = users[i];
        if (obj.login === user.login && obj.pass === user.pass) {
            return tokenServ.singToken(user);
        }
    }

    return false;


}

//------auth end

//----mongoWork---start
const mongoClient = new Mongo();
app.post('/api/book', function (req, res) {
    var data = [];
    req.on('data', function (chunk) {
        data.push(chunk);
    });
    req.on('end', function () {
        data = Buffer
            .concat(data)
            .toString();
        const obj = JSON.parse(data);
        mongoClient.insertDoc(obj, function () {
            res.sendStatus(201);
        }, function () {
            res.sendStatus(500);
        });
    });

});
app.get('/api/book', function (req, res) {
    guard(req, 1, function () {
        mongoClient.getAllColection(function (docs) {
            res.json(JSON.stringify(docs));
        }, function () {
            res.sendStatus(500);
        });
    }, function (message) {
        console.log(message);
        res.status(403);
    });
});
app.post('/api/update/books', function (req, res) {
    var data = [];
    req.on('data', function (chunk) {
        data.push(chunk);
    });
    req.on('end', function () {
        data = Buffer
            .concat(data)
            .toString();
        const obj = JSON.parse(data);
        guard(req, 1, function () {
            mongoClient.updateStatusItemsFromCollection(obj, function () {
                res.sendStatus(201);
            }, function () {
                res.sendStatus(500);
            });

        }, function (message) {
            console.log(message);
            res.status(403);
        });
    });

});
app.post('/api/update/proclist', function (req, res) {
    var data = [];
    req.on('data', function (chunk) {
        data.push(chunk);
    });
    req.on('end', function () {
        data = Buffer
            .concat(data)
            .toString();
        const objs = JSON.parse(data);
        guard(req, 1, function () {
            mongoClient.updateStatusForProcurementList(objs, function () {
                res.sendStatus(201);
            }, function () {
                res.sendStatus(500);
            });

        }, function (message) {
            console.log(message);
            res.status(403);
        });
    });

});
app.get('/api/proclist', function (req, res) {
    guard(req, 1, function () {
        mongoClient.getForProcurementList(function (docs) {
            res.json(JSON.stringify(docs));
        }, function (err) {
            res.status(500).send(err.message);
        });
    }, function (message) {
        console.log(message);
        res.status(403);
    });
});
app.post('/api/mark', function (req, res) {
    var data = [];
    req.on('data', function (chunk) {
        data.push(chunk);
    });
    req.on('end', function () {
        data = Buffer
            .concat(data)
            .toString();
        const obj = JSON.parse(data);
        mongoClient.addBookMark(obj, function () {
            res.sendStatus(201);
        }, function () {
            res.sendStatus(500);
        });
    });

});
app.post('/api/removemark', function (req, res) {
    var data = [];
    req.on('data', function (chunk) {
        data.push(chunk);
    });
    req.on('end', function () {
        data = Buffer
            .concat(data)
            .toString();
        const obj = JSON.parse(data);
        mongoClient.removeBookMark(obj, function () {
            res.sendStatus(201);
        }, function () {
            res.sendStatus(500);
        });
    });

});
app.post('/api/marks', function (req, res) {
    var data = [];
    req.on('data', function (chunk) {
        data.push(chunk);
    });
    req.on('end', function () {
        data = Buffer
            .concat(data)
            .toString();
        const obj = JSON.parse(data);
        mongoClient.getMarks(obj, function (docs) {
            res.json(JSON.stringify(docs));
        }, function () {
            res.sendStatus(500);
        });
    });

});
//---mongoWork---end -------------------------------------

//----guard  ------ start -----
//permission - set max level permission (attr:role)
const guard = function (req, permission, callback, error) {
    const perm = Number(permission);
    if (req.session.token === undefined) {
        return error("no token in session");
    }
    const token = tokenServ.verifyToken(req.session.token);

    if (token === false) {
        delete req.session.token;
        return error("TOKEN ERROR");
    }
    if (Number(token.role) >= perm) {
        return callback();
    } else {
        return error("NO PERMISSION");
    }
}
//----guard  ------ ends -----

app.get('*', function (req, res) {
    res.redirect('/#/');
});