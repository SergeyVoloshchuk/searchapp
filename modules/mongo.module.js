function Mongo() {
    "use strict";
    const appConfig = require('./../config/app-config');
    const MongoClient = require('mongodb').MongoClient,
        assert = require('assert');
    const url = appConfig.mongoSettings.url + appConfig.mongoSettings.DBName;
    const colectionName = "books";
    const procurementList = "procurementList";
    const ObjectId = require('mongodb').ObjectID;
    //methods----------------------------------start
    const getAllObject = function (db, find, collection, callback, error, final) {
        const promise = new Promise((resolve, reject) => {
            collection
                .find(find)
                .toArray(function (err, docs) {
                    assert.equal(err, null);
                    if (err) {
                        reject(err);
                    } else {
                        resolve(docs);
                    }
                });
        });
        promise.then((docs) => {
            callback(docs);
        }).catch((err) => {
            error(err);
        }).then(() => {
            final();
        });
    }
    const insertDocumentOne = function (db, object, callback, error, final) {
        var collection = db.collection(colectionName);
        const promise = new Promise((resolve, reject) => {
            collection.insertOne(object, function (err, result) {
                assert.equal(err, null);
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        });
        promise.then(() => {
            callback();
        }).catch((err) => {
            error(err);
        }).then(() => {
            final();
        });
    }
    const updateStatus = function (db, objs, callback, error, final) {
        var collection = db.collection("books");
        const prom = new Promise((resolve, reject) => {
            if (objs.length < 1) {
                resolve();
            }
            var it = 0;
            for (var i = 0; i < objs.length; i++) {
                collection.update({
                    _id: ObjectId(objs[i]._id)
                }, {
                    $set: {
                        remove: objs[i].remove
                    }

                }, {
                    upsert: false
                }, function (err, result) {
                    assert.equal(err, null);
                    if (err) {
                        reject(err);
                    } else {
                        it = it + 1;
                        if (it === objs.length) {
                            resolve();
                        }
                    }
                });
            }
        });
        prom.then(() => {
            callback();
        }).catch((err) => {
            error(err);
        }).then(() => {
            final();
        });

    }
    const updateStatusProc = function (db, objs, callback, error, final) {
        var collection = db.collection("procurementList");
        const prom = new Promise((resolve, reject) => {
            if (objs.length < 1) {
                resolve();
            }
            var it = 0;
            for (var i = 0; i < objs.length; i++) {
                collection.update({
                    _id: ObjectId(objs[i]._id),
                    _book: objs[i]._book,
                    _user: objs[i]._user
                }, {
                    $set: {
                        remove: objs[i].remove,
                        status: objs[i].status
                    }

                }, {
                    upsert: true
                }, function (err, result) {
                    assert.equal(err, null);
                    if (err) {
                        reject(err);
                    } else {
                        it = it + 1;
                        if (it === objs.length) {
                            resolve();
                        }
                    }
                });
            }
        });
        prom.then(() => {
            callback();
        }).catch((err) => {
            error(err);
        }).then(() => {
            final();
        });

    }
    const addMark = function (db, object, callback, error, final) {
        var collection = db.collection(object._user);
        const promise = new Promise((resolve, reject) => {
            collection.insertOne(object, function (err, result) {
                assert.equal(err, null);
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        });
        promise.then(() => {
            callback();
        }).catch((err) => {
            error(err);
        }).then(() => {
            final();
        });
    }
    const removeMark = function (db, object, callback, error, final) {
        var collection = db.collection(object._user);
        object._book.saveBook = true;
        const promise = new Promise((resolve, reject) => {
            collection.deleteOne({
                _book: object._book
            }, function (err) {
                assert.equal(err, null);
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
        });
        promise.then(() => {
            callback();
        }).catch((err) => {
            error(err);
        }).then(() => {
            final();
        });
    }
    const getAllMarks = function (db, collection, callback, error, final) {
        const promise = new Promise((resolve, reject) => {
            collection
                .find()
                .toArray(function (err, docs) {
                    assert.equal(err, null);
                    if (err) {
                        reject(err);
                    } else {
                        resolve(docs);
                    }
                });
        });
        promise.then((docs) => {
            callback(docs);
        }).catch((err) => {
            error(err);
        }).then(() => {
            final();
        });
    }
    //methods----------------------------------end

    const insertDoc = function (obj, callback, error) {
        MongoClient.connect(url, function (err, db) {
            if (err) {
                console.error(err.message);
                return error();
            }
            insertDocumentOne(db, obj, function () {
                callback();
            }, function () {
                error();
            }, function () {
                db.close();
            });
        });
    }

    const getAllColection = function (callback, error) {
        const find = {
            remove: false,
            status: false
        };

        MongoClient
            .connect(url, function (err, db) {
                if (err) {
                    console.error(err.message);
                    return error(err);
                }
                const collection = db.collection(colectionName);
                getAllObject(db, find, collection, function (docs) {
                    callback(docs);
                }, function () {
                    error();
                }, function () {
                    db.close();
                });
            });
    }

    const updateStatusItemsFromCollection = function (objs, callback, error) {
        MongoClient.connect(url, function (err, db) {
            if (err) {
                console.error(err.message);
                return error(err);
            }
            updateStatus(db, objs, function () {
                callback();
            }, function () {
                error();
            }, function () {
                db.close();
            });
        });
    }

    const getForProcurementList = function (callback, error) {
        const find = {
            remove: false
        };
        MongoClient.connect(url, function (err, db) {
            if (err) {
                console.error(err.message);
                return error(err);
            }
            const collection = db.collection(procurementList);
            getAllObject(db, find, collection, function (docs) {
                callback(docs);
            }, function (err) {
                error(err);
            }, function () {
                db.close();
            });

        });
    }
    const updateStatusForProcurementList = function (objs, callback, error) {

        MongoClient.connect(url, function (err, db) {
            if (err) {
                console.error(err.message);
                return error(err);
            }
            updateStatusProc(db, objs, function () {
                callback();
            }, function (err) {
                error(err);
            }, function () {
                db.close();
            })
        });
    }

    const addBookMark = function (obj, callback, error) {
        MongoClient.connect(url, function (err, db) {
            if (err) {
                console.error(err.message);
                return error(err);
            }
            addMark(db, obj, function () {
                callback();
            }, function (err) {
                error(err);
            }, function () {
                db.close();
            })
        });
    }
    const removeBookMark = function (obj, callback, error) {
        MongoClient.connect(url, function (err, db) {
            if (err) {
                console.error(err.message);
                return error(err);
            }
            removeMark(db, obj, function () {
                callback();
            }, function (err) {
                error(err);
            }, function () {
                db.close();
            })
        });
    }
    const getMarks = function (userName, callback, error) {
        MongoClient
            .connect(url, function (err, db) {
                if (err) {
                    console.error(err.message);
                    return error(err);
                }
                const collection = db.collection(userName);
                getAllMarks(db, collection, function (docs) {
                    callback(docs);
                }, function () {
                    error();
                }, function () {
                    db.close();
                });
            });
    }

    return {
        insertDoc: insertDoc,
        getAllColection: getAllColection,
        updateStatusItemsFromCollection: updateStatusItemsFromCollection,
        getForProcurementList: getForProcurementList,
        updateStatusForProcurementList: updateStatusForProcurementList,
        addBookMark: addBookMark,
        removeBookMark: removeBookMark,
        getMarks: getMarks
    };

};

module.exports = Mongo;