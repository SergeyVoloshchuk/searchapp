function TokenService(key) {
    const jwt = require('jsonwebtoken');
    const _key = key;

    function singToken(obj) {
        const token = jwt.sign(obj, _key);
        return token;
    };

    function verifyToken(token) {
        try {
            const decoded = jwt.verify(token, _key);
            return decoded;
        } catch (err) {
            return false;
        }
    };

    return {
        singToken: singToken,
        verifyToken: verifyToken
    };
}
module.exports = TokenService;