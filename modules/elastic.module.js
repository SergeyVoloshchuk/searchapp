function ElasticSearch() {

    const _ = require('lodash');
    const config = require('./../config/configPartner.json');
    const mainConfig = require('./../config/app-config.json');
    const elasticsearch = require('elasticsearch');
    const noPicture = mainConfig.noImageUrl;
    const client = new elasticsearch.Client({
        host: mainConfig.elasticsearch.host,
        log: ['info', 'warning', 'error']
    });

    function partnerConfig(item) {
        var url = "";
        _.forEach(config.configs, function (config) {
            if (config.shop === item.shop) {
                url = item.url = item.url + config.url;
            } else {
                url = item.url;
            }
        });
        return url;
    }
    const suggest = function (data, callback, errorCallback) {
        data = data.substring(1, data.length - 1);
        var dataMass = _.split(data, ' ');
        var checkIndex = data.lastIndexOf(' ');
        var prefix = "";
        var pattern = "";
        var prefForSend = "";
        if (checkIndex === -1) {
            prefix = data;
            prefForSend = data;
        } else {
            if (dataMass.length > 2) {
                prefix = dataMass[dataMass.length - 2];

            } else {
                prefix = data.substring(0, checkIndex);
            }
            prefForSend = data.substring(0, checkIndex);

        }
        pattern = data.substring(checkIndex + 1) + ".*";
        client.search({
            body: {
                "size": 0,
                "query": {
                    "prefix": {
                        "title": {
                            "value": prefix
                        }
                    }
                },
                "aggs": {
                    "autocomplete": {

                        "terms": {
                            "size": 5,
                            "field": "title",
                            "order": {
                                "_count": "desc"
                            },
                            "include": {
                                "pattern": pattern
                            }
                        }

                    }
                },
                "suggest": {
                    "didYouMean": {
                        "text": data,
                        "phrase": {
                            "field": "title"
                        }
                    }
                },

                _source: false
            }

        })
            .then(function (body) {
                var hits = body;
                var objs = hits.aggregations.autocomplete.buckets;
                var didYouMeanOptions = hits.suggest.didYouMean[0].options;
                if (checkIndex >= 0) {
                    for (var i = 0; i < objs.length; i++) {

                        objs[i].key = prefForSend + " " + objs[i].key;
                    }
                }
                if (objs.length === 0 && didYouMeanOptions.length > 0) {
                    var didYouMean = didYouMeanOptions[0];
                    callback(didYouMean);

                } else {
                    callback(objs);
                }

            }, function (error) {
                return errorCallback(error);
            });
    }
    const search = function (data, callback, errorCallback) {
        data = Buffer
            .concat(data)
            .toString("utf8");
        var re = /[\]\[\&\|\<\>\(\)\{\}\~\^\*\/\\]/gi;
        var str = JSON.parse(data);
        str = str.replace(re, '');
        client.search({
            body: {
                query: {
                    query_string: {
                        query: str
                    }
                },
                size: mainConfig.resultSize
            }

        })
            .then(function (body) {

                var hits = body;
                var objs = hits.hits.hits;
                var result = [];
                for (var i = 0; i < objs.length; i++) {
                    var book = {};
                    book.isbn = objs[i]._source.isbn;
                    book.id = objs[i]._source.id;
                    book.type = objs[i]._source.type;
                    book.name = objs[i]._source.name;
                    var massautor = _.split(objs[i]._source.author, ',');
                    book.author = massautor;
                    book.year = objs[i]._source.year;
                    book.publisher = objs[i]._source.publisher;
                    book.barcode = objs[i]._source.barcode;
                    book.description = objs[i]._source.description;
                    book.language = objs[i]._source.language;
                    book.price = objs[i]._source.price;
                    book.url = objs[i]._source.url;
                    book.shop = objs[i]._source.shop;
                    book.picture = objs[i]._source.picture
                    //no picture or picture - mass
                    if (typeof book.picture !== "string" && book.picture !== undefined) {
                        var pic = book.picture[0];
                        book.picture = pic;
                    }
                    if (book.picture === undefined || book.picture === "") {
                        book.picture = noPicture;
                    }
                    book.tags = objs[i]._source.tags;
                    book.cloneBook = [];
                    book.groupId = 0;
                    book.url = partnerConfig(book);
                    if (book.type === mainConfig.indexType) {
                        result.push(book);
                    }

                }
                callback(result);

            }, function (error) {
                return errorCallback(error);
            });

    }
    return {
        suggest: suggest,
        search: search
    };
}
module.exports = ElasticSearch;