(function(args) {
    console.log("ok...wait...");
    const mainConfig = require('./config/app-config');
    var name = process.argv[2];
    var fileName = process.argv[3];
    var batchSize = process.argv[4]
    var errorsName = [undefined, ''];
    for (var i = 0; i < errorsName.length; i++) {
        if (name === errorsName[i]) {
            console.log("Name index " + name + " is not correct");
            return;
        }


        if (fileName === errorsName[i]) {
            console.log("fileName " + fileName + " is not correct");
            return;
        }
        if (batchSize === errorsName[i]) {
            console.log("Number batchSize  is not correct");
            return;
        }
    }
    console.log("process starting...");

    var _ = require('lodash');
    var assert = require('assert');
    var rp = require('request-promise');

    var elasticsearch = require('elasticsearch');
    var client = new elasticsearch.Client({
        host: mainConfig.elasticsearch.host,
        log: ['error', 'info']

    });

    var fs = require('fs')
    var XmlStream = require('xml-stream');
    var stream = fs.createReadStream('./xml_package/' + process.argv[3])
    var xml = new XmlStream(stream);

    var massBooks = [];

    xml.on('endElement: offer', function(item) {
        createBook(item);
    });

    // on/off in app-config -----------------------
    var indexName = mainConfig.indexName;
    if (mainConfig.cleverParserMod) {
        initIndex(); //clever mode
    }
    // end app-config



    xml.on('end', function() {
        if (massBooks.length > 0) {
            xml.pause();
            indexing(massBooks);
        }
        console.log("process ended...");
    });



    function createBook(item) {
        var book = {};
        book.isbn = item['ISBN'];
        book.id = item['$']['id'];
        book.type = item['$']['type'];
        book.name = item['name'];
        book.author = item['author'];
        book.year = item['year'];
        book.publisher = item['publisher'];
        book.description = item['description'];
        book.language = item['language'];
        book.price = item['price'];
        book.url = item['url'];
        book.shop = process.argv[2];
        for (var prop in book) {
            if (book[prop] === undefined || book[prop] === "") {
                book[prop] = "Не указан";
            }
        }
        book.picture = item['picture'];
        massBooks.push(book);
        if (massBooks.length === Number(batchSize)) {
            xml.pause();
            indexing(massBooks);
            massBooks = [];
        }
    }

    function indexing(mass) {
        var body = [];
        for (var i = 0; i < mass.length; i++) {
            var item = mass[i];
            var massIbsn = _.split(item.isbn, ',');
            var index = {
                index: {
                    _index: indexName,
                    _type: item.shop,
                    _id: item.id,
                }
            };
            var other = {

                title: item.name,
                tags: massIbsn,
                isbn: item.isbn,
                id: item.id,
                type: item.type,
                name: item.name,
                author: item.author,
                year: item.year,
                publisher: item.publisher,
                description: item.description,
                language: item.language,
                price: item.price,
                url: item.url,
                shop: item.shop,
                picture: item.picture
            }

            body.push(index, other);
        }
        client.bulk({
            body: body
        }, function(err, resp) {
            xml.resume();
        });
    }



    function initIndex() {
        var settings = {
            "settings": {
                "index": {
                    "analysis": {
                        "filter": {
                            "russian_stemmer": {
                                "type": "stemmer",
                                "language": "russian"
                            },
                            "autocompleteFilter": {
                                "max_shingle_size": "5",
                                "min_shingle_size": "2",
                                "type": "shingle"
                            },
                            "stopwords": {
                                "type": "stop",
                                "stopwords": ["_english_", "_russian_"]
                            }
                        },
                        "analyzer": {
                            "didYouMean": {
                                "filter": ["lowercase"],
                                "char_filter": ["html_strip"],
                                "type": "custom",
                                "tokenizer": "standard"
                            },
                            "autocomplete": {
                                "filter": ["lowercase", "autocompleteFilter"],
                                "char_filter": ["html_strip"],
                                "type": "custom",
                                "tokenizer": "standard"
                            },
                            "default": {
                                "filter": ["lowercase", "stopwords", "stemmer"],
                                "char_filter": ["html_strip"],
                                "type": "custom",
                                "tokenizer": "standard"
                            }
                        }
                    }
                }
            }

        };
        var mapping = {
            "properties": {
                "title": {
                    "type": "string",
                    "analyzer": "autocomplete"
                },
                "description": {
                    "type": "string",
                    "analyzer": "didYouMean"
                }
            }
        };
        client.indices.exists({
            index: indexName
        }, function(err, resp, respcode) {
            if (!resp) {
                createIndexAndSettings();
            } else {
                createMapping();
            }
        });

        function createIndexAndSettings() {
            client.indices.create({
                index: indexName,
                body: settings
            }, function(err, resp, respcode) {
                createMapping();
            });
        }

        function createMapping() {
            client.indices.putMapping({
                index: indexName,
                type: process.argv[2],
                body: mapping
            }, function(err, resp, respcode) {

            });
        }

    }
})();