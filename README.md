# SearchBooks

## Requirements
- Install Node
    - https://nodejs.org/en/
- Install ElasticSearch 2.4
    - https://www.elastic.co/downloads/elasticsearch
- Install MongoDB
    - https://www.mongodb.com/

## Installing Node.js Packages
- Open terminal in your directory
- Type `npm install`

## Quick Start
- Open terminal in your directory
- Type `npm install`
- Starting ElasticSearch and check it. Stardart Host: http://localhost:9200
- Starting MongoDB and check it.
- Update config (`SearchBooks/config/app-config.json`)
- Check config `webpack.config.js`
### Developer
- Type `npm run dev`
- Type `npm start`
### Building
  - Type `npm run build`
### Build project and run
  - Type `npm run build:run`

### Commands
- Type `npm start` - Start prodaction Server
- Type `npm run dev` - Start Webpack Developer Server
- Type `npm run build` - Build with webpack in /dist
- Type `npm run build:run` - Build in dist and starting prodaction server

## Running parser
- Open terminal in your directory
- If there is no `xml_package` folder, create it. Place your xml, yml files there.
- Type `node util.js yourIndexName nameFile.xml stakCount`

## Config partner
- `configPartner.json`
``` javascript
    {
        "shop": "yourShopName",
        "url": "URl"
    }
```

