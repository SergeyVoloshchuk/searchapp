import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ResultComponent } from '../components/result.component/result.component';
import { NotFoundComponent } from '../components/not-found.component/not-found.component';
import { SearchComponent } from '../components/search.component/search.component';
import { ListComponent } from '../components/list.component/list.component';
import { ListGuard } from '../guards/list.guard';
import { ExitListGuard } from '../guards/exit.list.guard';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { AuthGuard } from '../guards/auth.guard';
import { AuthService } from '../services/auth.service';
import { ShoppingComponent } from './../components/shopping.component/shopping.component';
const routes: Routes = [
	{ path: '', component: SearchComponent ,canActivate: [AuthGuard] },
	{ path: 'result', redirectTo: '', pathMatch: 'full' },
	{ path: 'list', component: ListComponent, canActivate: [ListGuard], canDeactivate: [ExitListGuard],pathMatch: 'full' },
	{ path: 'shopping', component: ShoppingComponent, canActivate: [ListGuard],pathMatch: 'full' },
	{ path: '**', component: NotFoundComponent }
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule],
	providers: [AuthService,AuthGuard, ListGuard, ExitListGuard, { provide: LocationStrategy, useClass: HashLocationStrategy }],

})
export class AppRoutingModule { }