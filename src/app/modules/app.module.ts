import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { AppComponent } from '../components/app.component/app.component';
import { HttpModule } from '@angular/http';
import { ResultComponent } from '../components/result.component/result.component';
import { SortPipe } from '../pipes/orderByBook.pipe';
import { AppRoutingModule } from './app.routing.module';
import { NotFoundComponent } from '../components/not-found.component/not-found.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SearchComponent } from '../components/search.component/search.component';
import { ListComponent } from '../components/list.component/list.component';
import { Ng2DragDropModule } from "ng2-drag-drop";
import { LoginComponent } from '../components/login.component/login.component';
import { AuthService } from '../services/auth.service';
import { Ng2Bs3ModalModule } from 'ng2-bs3-modal/ng2-bs3-modal';
import { ToasterModule, ToasterService } from 'angular2-toaster';
import { StorageService } from './../services/storage.service';
import { ScrollDirective } from './../directives/scroll.directive';
import { ShoppingComponent } from './../components/shopping.component/shopping.component';
const imports = [
	BrowserModule,
	Ng2Bs3ModalModule,
	FormsModule,
	HttpModule,
	AppRoutingModule,
	ReactiveFormsModule,
	Ng2DragDropModule,
	ToasterModule
];
const declarations = [
	AppComponent,
	ResultComponent,
	SortPipe,
	NotFoundComponent,
	SearchComponent,
	ListComponent,
	LoginComponent,
	ScrollDirective,
	ShoppingComponent
];
@NgModule({
	imports: imports,
	declarations: declarations,
	bootstrap: [AppComponent],
	providers: [AuthService, ToasterService, StorageService]
})
export class AppModule { }
