"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var forms_1 = require("@angular/forms");
var app_component_1 = require("../components/app.component");
var http_1 = require("@angular/http");
var result_component_1 = require("../components/result.component");
var orderByBook_pipe_1 = require("../pipes/orderByBook.pipe");
var app_routing_module_1 = require("./app.routing.module");
var not_found_component_1 = require("../components/not-found.component");
var forms_2 = require("@angular/forms");
var search_component_1 = require("../components/search.component");
var list_component_1 = require("../components/list.component");
var ng2_drag_drop_1 = require("ng2-drag-drop");
var login_component_1 = require("../components/login.component");
var auth_service_1 = require("../services/auth.service");
var ng2_bs3_modal_1 = require("ng2-bs3-modal/ng2-bs3-modal");
var angular2_toaster_1 = require("angular2-toaster");
var imports = [
    platform_browser_1.BrowserModule,
    ng2_bs3_modal_1.Ng2Bs3ModalModule,
    forms_1.FormsModule,
    http_1.HttpModule,
    app_routing_module_1.AppRoutingModule,
    forms_2.ReactiveFormsModule,
    ng2_drag_drop_1.Ng2DragDropModule,
    angular2_toaster_1.ToasterModule
];
var declarations = [
    app_component_1.AppComponent,
    result_component_1.ResultComponent,
    orderByBook_pipe_1.SortPipe,
    not_found_component_1.NotFoundComponent,
    search_component_1.SearchComponent,
    list_component_1.ListComponent,
    login_component_1.LoginComponent
];
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        imports: imports,
        declarations: declarations,
        bootstrap: [app_component_1.AppComponent],
        providers: [auth_service_1.AuthService, angular2_toaster_1.ToasterService]
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map