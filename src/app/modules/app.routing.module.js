"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var not_found_component_1 = require("../components/not-found.component");
var search_component_1 = require("../components/search.component");
var list_component_1 = require("../components/list.component");
var list_guard_1 = require("../guards/list.guard");
var exit_list_guard_1 = require("../guards/exit.list.guard");
var common_1 = require("@angular/common");
var auth_guard_1 = require("../guards/auth.guard");
var auth_service_1 = require("../services/auth.service");
var routes = [
    { path: '', component: search_component_1.SearchComponent, canActivate: [auth_guard_1.AuthGuard] },
    { path: 'result', redirectTo: '', pathMatch: 'full' },
    { path: 'list', component: list_component_1.ListComponent, canActivate: [list_guard_1.ListGuard], canDeactivate: [exit_list_guard_1.ExitListGuard], pathMatch: 'full' },
    { path: '**', component: not_found_component_1.NotFoundComponent }
];
var AppRoutingModule = (function () {
    function AppRoutingModule() {
    }
    return AppRoutingModule;
}());
AppRoutingModule = __decorate([
    core_1.NgModule({
        imports: [router_1.RouterModule.forRoot(routes)],
        exports: [router_1.RouterModule],
        providers: [auth_service_1.AuthService, auth_guard_1.AuthGuard, list_guard_1.ListGuard, exit_list_guard_1.ExitListGuard, { provide: common_1.LocationStrategy, useClass: common_1.HashLocationStrategy }],
    })
], AppRoutingModule);
exports.AppRoutingModule = AppRoutingModule;
//# sourceMappingURL=app.routing.module.js.map