export class User {
    private _token : string;
    private _user : string;
    constructor(token : string, user : string) {
        this._user = user;
        this._token = token;
    }
  
    get token() : string {return this._token;}
    set token(token : string) {
        this._token= token;
    }
    get user() : string {return this._user;}
    set user(user : string) {
        this._user = user;
    }

}