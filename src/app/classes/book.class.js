"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Book = (function () {
    function Book() {
        this._cloneBook = [];
    }
    Object.defineProperty(Book.prototype, "isbn", {
        get: function () {
            return this._isbn;
        },
        set: function (isbn) {
            this._isbn = isbn;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Book.prototype, "id", {
        get: function () {
            return this._id;
        },
        set: function (id) {
            this._id = id;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Book.prototype, "type", {
        get: function () {
            return this._type;
        },
        set: function (type) {
            this._type = type;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Book.prototype, "name", {
        get: function () {
            return this._name;
        },
        set: function (name) {
            this._name = name;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Book.prototype, "author", {
        get: function () {
            return this._author;
        },
        set: function (author) {
            this._author = author;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Book.prototype, "year", {
        get: function () {
            return this._year;
        },
        set: function (year) {
            this._year = year;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Book.prototype, "publisher", {
        get: function () {
            return this._publisher;
        },
        set: function (publisher) {
            this._publisher = publisher;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Book.prototype, "barcode", {
        get: function () {
            return this._barcode;
        },
        set: function (barcode) {
            this._barcode = barcode;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Book.prototype, "description", {
        get: function () {
            return this._description;
        },
        set: function (description) {
            this._description = description;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Book.prototype, "language", {
        get: function () {
            return this._language;
        },
        set: function (language) {
            this._language = language;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Book.prototype, "price", {
        get: function () {
            return this._price;
        },
        set: function (price) {
            this._price = price;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Book.prototype, "url", {
        get: function () {
            return this._url;
        },
        set: function (url) {
            this._url = url;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Book.prototype, "shop", {
        get: function () {
            return this._shop;
        },
        set: function (shop) {
            this._shop = shop;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Book.prototype, "picture", {
        get: function () {
            return this._picture;
        },
        set: function (picture) {
            this._picture = picture;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Book.prototype, "tags", {
        get: function () {
            return this._tags;
        },
        set: function (tags) {
            this._tags = tags;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Book.prototype, "cloneBook", {
        get: function () {
            return this._cloneBook;
        },
        set: function (cloneBook) {
            this._cloneBook = cloneBook;
        },
        enumerable: true,
        configurable: true
    });
    return Book;
}());
exports.Book = Book;
//# sourceMappingURL=book.class.js.map