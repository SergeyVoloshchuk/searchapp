export class Book {
    private _isbn: string;
    private _id: string;
    private _type: string;
    private _name: string;
    private _author: string;
    private _year: string;
    private _publisher: string;
    private _barcode: string;
    private _description: string;
    private _language: string;
    private _price: number;
    private _url: string;
    private _shop: string;
    private _picture: string;
    private _tags: Array<string>;
    private _cloneBook: Array<Book>;
    private _saveBook: boolean = false;
    private _dialogForSave: boolean = false;

    constructor() {
        this._cloneBook = [];
    }
    public get isbn(): string {
        return this._isbn;
    }
    public set isbn(isbn: string) {
        this._isbn = isbn;
    }

    public get id(): string {

        return this._id;
    }
    public set id(id: string) {
        this._id = id;
    }

    public get type(): string {
        return this._type;
    }
    public set type(type: string) {
        this._type = type;
    }

    public get name(): string {
        return this._name;
    }
    public set name(name: string) {
        this._name = name;
    }

    public get author(): string {
        return this._author;
    }
    public set author(author: string) {
        this._author = author;
    }

    public get year(): string {
        return this._year;
    }
    public set year(year: string) {
        this._year = year;
    }

    public get publisher(): string {
        return this._publisher;
    }
    public set publisher(publisher: string) {
        this._publisher = publisher;
    }
    public get barcode(): string {
        return this._barcode;
    }
    public set barcode(barcode: string) {
        this._barcode = barcode;
    }
    public get description(): string {
        return this._description;
    }
    public set description(description: string) {
        this._description = description;
    }
    public get language(): string {
        return this._language;
    }
    public set language(language: string) {
        this._language = language;
    }
    public get price(): number {
        return this._price;
    }
    public set price(price: number) {
        this._price = price;
    }
    public get url(): string {
        return this._url;
    }
    public set url(url: string) {
        this._url = url;
    }
    public get shop(): string {
        return this._shop;
    }
    public set shop(shop: string) {
        this._shop = shop;
    }

    public get picture(): string {

        return this._picture;
    }

    public set picture(picture: string) {
        this._picture = picture;
    }

    public get tags(): Array<string> {
        return this._tags;
    }
    public set tags(tags: Array<string>) {
        this._tags = tags;
    }

    public get cloneBook(): Array<Book> {
        return this._cloneBook;
    }
    public set cloneBook(cloneBook: Array<Book>) {
        this._cloneBook = cloneBook;
    }
    public get saveBook(): boolean {
        return this._saveBook;
    }
    public set saveBook(saveBook: boolean) {
        this._saveBook = saveBook;
    }
    public get dialogForSave(): boolean {
        return this._dialogForSave;
    }
    public set dialogForSave(dialogForSave: boolean) {
        this._dialogForSave = dialogForSave;
    }



}