"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var User = (function () {
    function User(token, user) {
        this._user = user;
        this._token = token;
    }
    Object.defineProperty(User.prototype, "token", {
        get: function () { return this._token; },
        set: function (token) {
            this._token = token;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(User.prototype, "user", {
        get: function () { return this._user; },
        set: function (user) {
            this._user = user;
        },
        enumerable: true,
        configurable: true
    });
    return User;
}());
exports.User = User;
//# sourceMappingURL=user.class.js.map