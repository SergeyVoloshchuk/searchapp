import { Book } from "./book.class";
export class Sentence {
    readonly _book: Book;
    readonly _id: string;
    readonly _user: string;
    public remove: boolean;
    public status: boolean;
    public clone: Array<Book>;
    constructor(book: Book, user: string) {
        this._user = user;
        this._book = book;
        this.remove = false;
        this.status = false;
        this.clone = [];
    }

}