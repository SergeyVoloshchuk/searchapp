"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Sentence = (function () {
    function Sentence(book, user) {
        this._user = user;
        this._book = book;
        this.remove = false;
        this.status = false;
        this.clone = [];
    }
    return Sentence;
}());
exports.Sentence = Sentence;
//# sourceMappingURL=sentence.class.js.map