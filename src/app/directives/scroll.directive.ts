import { Directive, ElementRef, HostListener, Renderer, Input } from '@angular/core';

@Directive({
    selector: '[scroll]',
    host: {
        '(window:scroll)': 'onScroll()'
    }
})
export class ScrollDirective {
    private position: number;
    constructor(private element: ElementRef, private renderer: Renderer) {
    }
    @Input("scrollClass") className: string;
    @Input("positionForFix") posForFix: number = 0;
    ngOnInit() {
        if (this.posForFix !== 0) {
            this.position = this.posForFix;
        }
        else {
            this.position = this.element.nativeElement.getBoundingClientRect().top;
        }

    }
    @HostListener("onscroll") onScroll() {
        if (scrollY > this.position) {
            this.renderer.setElementClass(this.element.nativeElement, this.className, true);
        }
        else {
            this.renderer.setElementClass(this.element.nativeElement, this.className, false);
        }

    }




}