import { Component, NgZone, OnInit, ViewChild, HostListener } from '@angular/core';
import { HttpService } from '../../services/http.service';
import { Book } from '../../classes/book.class';
import { SpeechRecognitionService } from "../../services/speech.service"
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { ActivatedRoute } from '@angular/router';
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';
import { Subject } from 'rxjs/Subject';
import { ToasterModule, ToasterService } from 'angular2-toaster';
@Component({
    selector: 'search',
    templateUrl: "./search.template.html",
    providers: [
        HttpService, SpeechRecognitionService
    ],
    styleUrls: ['./search.less']
})

export class SearchComponent {

    private speechData: string;
    private receivedBooks: Book[] = [];
    private suggestOptions: any = [];
    private text: string;
    private caret: boolean = true;
    private onCaret: boolean = false;
    private didYouMean: string;
    private load: boolean = false;
    private loadBooks: boolean = false;
    private noResult: boolean = false;
    private focusInput: boolean = false;
    private toasterService: ToasterService;
    @ViewChild('modalError')
    modal: ModalComponent;
    private querySubscription: Subscription;
    constructor(toasterService: ToasterService, private zone: NgZone, private route: ActivatedRoute, private router: Router, private httpService: HttpService, private speechRecognitionService: SpeechRecognitionService) {
        this.toasterService = toasterService;
    }

    ngOnInit() {
        this.querySubscription = this
            .route
            .queryParams
            .subscribe((queryParam: any) => {
                this.receivedBooks = [];
                this.text = queryParam['searchText'];
                this.search(this.text);
                this.suggestOptions = [];
                this.didYouMean = undefined;
                this.noResult = false;

            });

    }
    fl: boolean = true;
    changeCaret() {
        this
            .zone
            .run(() => {
                this.caret = !this.caret;
                this.onCaret = !this.onCaret;
            });
    }
    clickForSuggest(item: string) {
        if (item !== "Не указан") {
            this.text = item;
            this.suggestOptions = [];
            this.didYouMean = undefined;
            this.search(this.text);
        }


    }

    activateSpeechSearchMovie(): void {
        if (this.onCaret) {
            this
                .speechRecognitionService
                .DestroySpeechObject();
            return;
        }
        this.changeCaret();
        try {
            this
                .speechRecognitionService
                .record()
                .subscribe((value) => {
                    this.text = value;
                    this.search(this.text);
                }, (err) => {
                    if (err.error == "no-speech") {
                        this.toasterService.pop('warning', "Голосовой поиск", "Проверьте микрофон и уровень громкости.");
                    }
                    this.changeCaret();
                }, () => {
                    this.changeCaret();
                    this
                        .speechRecognitionService
                        .DestroySpeechObject();
                });
        } catch (Exeption) {
            //It is not supported by your browser
            this.caret = false;
            this.onCaret = false;
            this.modal.open();
        }

    }

    onKey(value: string) {
        this.text = value;
        this.search(this.text);
    }
    onBlur() {
        if (!this.focusInput) {
            this.suggestOptions = [];
            this.didYouMean = undefined;
        }
    }
    onFocus() {
        this.focusInput = true;
    }
    outFocus() {
        this.focusInput = false;
    }
    keyUp(value: string, event: any) {
        this.didYouMean = undefined;
        if (value !== "" && event.key !== "Enter") {

            this.suggest(value);

        } else {

            this.suggestOptions = [];
        }
    }

    suggest(text: string) {
        this.load = true;
        text = text.toLowerCase();
        this
            .httpService
            .postSuggest(text)
            .subscribe((data) => {
                let bufferData = JSON.parse(data);
                if (bufferData.text !== undefined) {
                    this.didYouMean = bufferData.text;
                } else {
                    this.suggestOptions = bufferData;
                }

            }, (err) => {
                this.toasterService.pop('error', 'Ошибка сервера', err._body);
                this.load = false;
            }, () => {
                this.load = false;
            });
    }

    search(text: string) {
        if (text !== "" && text) {
            this.loadBooks = true;
            this
                .router
                .navigate(['result'], {
                    queryParams: {
                        'searchText': text
                    }
                });
            this
                .httpService
                .postData(text)
                .subscribe((data) => {
                    this.receivedBooks = JSON.parse(data);
                }, (err) => {
                    this.toasterService.pop('error', "Ошибка сервера", err._body);
                    this.loadBooks = false;
                }, () => {
                    this.suggestOptions = [];
                    if (this.receivedBooks.length === 0) {
                        this.noResult = true;
                    }
                    this.loadBooks = false;
                });

        }

    }

    plusSuggest(event: any, text: string, input: any) {
        this.text = "";
        this.text += text;
        input.focus();
        this.suggest(this.text);
        event.stopPropagation();

    }
}
