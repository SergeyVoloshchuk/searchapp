"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_service_1 = require("../services/http.service");
var speech_service_1 = require("../services/speech.service");
var router_1 = require("@angular/router");
var router_2 = require("@angular/router");
var ng2_bs3_modal_1 = require("ng2-bs3-modal/ng2-bs3-modal");
var SearchComponent = (function () {
    function SearchComponent(zone, route, router, httpService, speechRecognitionService) {
        this.zone = zone;
        this.route = route;
        this.router = router;
        this.httpService = httpService;
        this.speechRecognitionService = speechRecognitionService;
        this.receivedBooks = [];
        this.suggestOptions = [];
        this.caret = true;
        this.onCaret = false;
        this.load = false;
        this.loadBooks = false;
        this.noResult = false;
    }
    SearchComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.querySubscription = this
            .route
            .queryParams
            .subscribe(function (queryParam) {
            _this.receivedBooks = [];
            _this.text = queryParam['searchText'];
            _this.search(_this.text);
            _this.suggestOptions = [];
            _this.didYouMean = undefined;
            _this.noResult = false;
        });
    };
    SearchComponent.prototype.changeCaret = function () {
        var _this = this;
        this
            .zone
            .run(function () {
            _this.caret = !_this.caret;
            _this.onCaret = !_this.onCaret;
        });
    };
    SearchComponent.prototype.clickForSuggest = function (item) {
        if (item !== "Не указан") {
            this.text = item;
            this.suggestOptions = [];
            this.didYouMean = undefined;
            this.search(this.text);
        }
    };
    SearchComponent.prototype.activateSpeechSearchMovie = function () {
        var _this = this;
        if (this.onCaret) {
            this
                .speechRecognitionService
                .DestroySpeechObject();
            return;
        }
        this.changeCaret();
        try {
            this
                .speechRecognitionService
                .record()
                .subscribe(function (value) {
                _this.text = value;
                _this.search(_this.text);
            }, function (err) {
                if (err.error == "no-speech") {
                    //no-speech
                }
                _this.changeCaret();
            }, function () {
                _this.changeCaret();
                _this
                    .speechRecognitionService
                    .DestroySpeechObject();
            });
        }
        catch (Exeption) {
            //It is not supported by your browser
            this.caret = false;
            this.onCaret = false;
            this.modal.open();
        }
    };
    SearchComponent.prototype.onKey = function (value) {
        this.text = value;
        this.search(this.text);
    };
    SearchComponent.prototype.onBlur = function () {
        this.suggestOptions = [];
        this.didYouMean = undefined;
    };
    SearchComponent.prototype.keyUp = function (value, event) {
        this.didYouMean = undefined;
        if (value !== "" && event.key !== "Enter") {
            this.suggest(value);
        }
        else {
            this.suggestOptions = [];
        }
    };
    SearchComponent.prototype.suggest = function (text) {
        var _this = this;
        this.load = true;
        this
            .httpService
            .postSuggest(text)
            .subscribe(function (data) {
            var bufferData = JSON.parse(data);
            if (bufferData.text !== undefined) {
                _this.didYouMean = bufferData.text;
            }
            else {
                _this.suggestOptions = bufferData;
            }
        }, function (err) { }, function () {
            _this.load = false;
        });
    };
    SearchComponent.prototype.search = function (text) {
        var _this = this;
        if (text !== "" && text) {
            this.loadBooks = true;
            this
                .router
                .navigate(['result'], {
                queryParams: {
                    'searchText': text
                }
            });
            this
                .httpService
                .postData(text)
                .subscribe(function (data) {
                _this.receivedBooks = JSON.parse(data);
            }, function (err) { }, function () {
                _this.suggestOptions = [];
                if (_this.receivedBooks.length === 0) {
                    _this.noResult = true;
                }
                _this.loadBooks = false;
            });
        }
    };
    SearchComponent.prototype.plusSuggest = function (event, text, input) {
        this.text = "";
        this.text += text;
        input.focus();
        this.suggest(this.text);
    };
    return SearchComponent;
}());
__decorate([
    core_1.ViewChild('modalError'),
    __metadata("design:type", ng2_bs3_modal_1.ModalComponent)
], SearchComponent.prototype, "modal", void 0);
SearchComponent = __decorate([
    core_1.Component({
        selector: 'search',
        templateUrl: "../app/templates/search.template.html",
        providers: [
            http_service_1.HttpService, speech_service_1.SpeechRecognitionService
        ],
        styleUrls: ['app/css/style.css', 'app/css/search.css']
    }),
    __metadata("design:paramtypes", [core_1.NgZone, router_2.ActivatedRoute, router_1.Router, http_service_1.HttpService, speech_service_1.SpeechRecognitionService])
], SearchComponent);
exports.SearchComponent = SearchComponent;
//# sourceMappingURL=search.component.js.map