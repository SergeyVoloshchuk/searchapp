"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var user_class_1 = require("../classes/user.class");
var sentence_class_1 = require("../classes/sentence.class");
var db_service_1 = require("../services/db.service");
var auth_service_1 = require("../services/auth.service");
var angular2_toaster_1 = require("angular2-toaster");
var ResultComponent = (function () {
    function ResultComponent(db, auth, toasterService) {
        this.db = db;
        this.auth = auth;
        this.state = 'inactive';
        this.noItemMessage = "Не указан";
        this.receivedBooks = [];
        this.search = new core_1.EventEmitter();
        this.toasterService = toasterService;
    }
    ResultComponent.prototype.toggleMove = function () {
        this.state = (this.state === 'inactive' ? 'active' : 'inactive');
    };
    Object.defineProperty(ResultComponent.prototype, "text", {
        get: function () {
            return this._text;
        },
        set: function (text) {
            this._text = text;
        },
        enumerable: true,
        configurable: true
    });
    ResultComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.auth
            .getIsLogin()
            .subscribe(function (value) {
            _this.basket = value;
        });
    };
    ResultComponent.prototype.addSentenceTost = function (name) {
        this.toasterService.pop('info', 'Добавлено', name + ' добавлен в список!');
    };
    ResultComponent.prototype.userErrorToast = function () {
        this.toasterService.pop('error', 'Ошибка', 'Не удалось считать данные пользователя!Повторите вход!');
    };
    ResultComponent.prototype.serverErrorToast = function () {
        this.toasterService.pop('error', 'Ошибка сервера', 'Не удалось отправить данные!');
    };
    ResultComponent.prototype.checkUser = function () {
        var localName = "usr";
        if (localStorage.getItem(localName) !== null) {
            try {
                var localUser = JSON.parse(localStorage.getItem(localName));
                var user = new user_class_1.User(localUser.token, localUser.user);
                return user;
            }
            catch (e) {
                return null;
            }
        }
        else
            return null;
    };
    ResultComponent.prototype.clickForFind = function (item) {
        if (item !== this.noItemMessage) {
            this.text = item;
            this
                .search
                .emit(this.text);
            scroll(0, 0);
        }
    };
    ResultComponent.prototype.addSentence = function (item) {
        var _this = this;
        item.cloneBook = [];
        this.user = this.checkUser();
        if (this.user === null) {
            this.userErrorToast();
            return this.auth.postLogout();
        }
        var obj = new sentence_class_1.Sentence(item, this.user.user);
        this.db
            .postBook(obj)
            .subscribe(function (value) { _this.addSentenceTost(item.name); }, function (err) { ; _this.serverErrorToast(); });
    };
    return ResultComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Array)
], ResultComponent.prototype, "receivedBooks", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Boolean)
], ResultComponent.prototype, "noResult", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Boolean)
], ResultComponent.prototype, "loadBooks", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], ResultComponent.prototype, "search", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", String),
    __metadata("design:paramtypes", [String])
], ResultComponent.prototype, "text", null);
ResultComponent = __decorate([
    core_1.Component({
        selector: 'result',
        templateUrl: "../app/templates/books-result.template.html",
        styleUrls: [
            'app/css/style.css', 'app/css/book-result.css'
        ],
        animations: [
            core_1.trigger('focusPanel', [
                core_1.state('inactive', core_1.style({
                    transform: 'scale(1)',
                    backgroundColor: '#eee'
                })),
                core_1.state('active', core_1.style({
                    transform: 'scale(1.1)',
                    backgroundColor: '#cfd8dc'
                })),
                core_1.transition('inactive => active', core_1.animate('100ms ease-in')),
                core_1.transition('active => inactive', core_1.animate('100ms ease-out'))
            ]),
            core_1.trigger('movePanel', [
                core_1.transition('void => *', [
                    core_1.animate(600, core_1.keyframes([
                        core_1.style({ opacity: 0, transform: 'translateY(-200px)', offset: 0 }),
                        core_1.style({ opacity: 1, transform: 'translateY(25px)', offset: .75 }),
                        core_1.style({ opacity: 1, transform: 'translateY(0)', offset: 1 }),
                    ]))
                ])
            ])
        ],
        providers: [db_service_1.DBService]
    }),
    __metadata("design:paramtypes", [db_service_1.DBService, auth_service_1.AuthService, angular2_toaster_1.ToasterService])
], ResultComponent);
exports.ResultComponent = ResultComponent;
//# sourceMappingURL=result.component.js.map