import { StorageService } from './../../services/storage.service';
import { Book } from './../../classes/book.class';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { User } from '../../classes/user.class';
import { Sentence } from '../../classes/sentence.class';
import { DBService } from '../../services/db.service';
import { AuthService } from '../../services/auth.service';
import { ToasterModule, ToasterService } from 'angular2-toaster';
@Component({
    selector: 'result',
    templateUrl: "./books-result.template.html",
    styleUrls: [
        './book-result.less'
    ],
    providers: [DBService, StorageService]

})

export class ResultComponent {
    private toasterService: ToasterService;

    constructor(private db: DBService, private auth: AuthService, toasterService: ToasterService, private storage: StorageService) {
        this.toasterService = toasterService;
    }
    private user: User;
    private basket: boolean;
    private _text: string;
    private noItemMessage: string = "Не указан";
    private marks: Array<Sentence> = [];
    @Input() receivedBooks: Book[] = [];
    @Input() noResult: boolean;
    @Input() loadBooks: boolean;
    @Output() search = new EventEmitter<string>();
    @Input()
    set text(text: string) {
        this._text = text;
    }
    get text() {
        return this._text;
    }
    ngOnInit() {
        this.auth
            .getIsLogin()
            .subscribe((value: boolean) => {
                this.basket = value;
                if (this.basket) {
                    this.getMarks();
                    this
                        .search
                        .emit(this.text);
                }
            });
    }
    addSentenceTost(name: string) {
        this.toasterService.pop('info', 'Добавлено', name + ' добавлен в список!');
    }
    userErrorToast() {
        this.toasterService.pop('error', 'Ошибка', 'Не удалось считать данные пользователя!Повторите вход!');
    }
    serverErrorToast() {
        this.toasterService.pop('error', 'Ошибка сервера', 'Не удалось отправить данные!');
    }
    checkUser(): User {
        const localName = "usr";
        const localUser = this.storage.getElementSrorage(localName);
        const user: User = new User(localUser.token, localUser.user);
        return user;
    }
    clickForFind(item: string) {
        if (item !== this.noItemMessage) {
            this.text = item;
            this
                .search
                .emit(this.text);
            scroll(0, 0);
        }
    }
    addSentence(item: Book) {
        item.cloneBook = [];
        this.user = this.checkUser();
        if (this.user === null) {
            this.userErrorToast();
            return this.auth.postLogout();
        }
        const obj: Sentence = new Sentence(item, this.user.user);
        this.db
            .postBook(obj)
            .subscribe((value) => { this.addSentenceTost(item.name); }, (err) => { ; this.serverErrorToast(); });

    }
    saveMark(item: Book) {
        item.dialogForSave = false;
        item.saveBook = true;
        this.user = this.checkUser();
        if (this.user === null) {
            this.userErrorToast();
            return this.auth.postLogout();
        }
        const obj: Sentence = new Sentence(item, this.user.user);
        this.db
            .postMark(obj)
            .subscribe((value) => {
                this.toasterService.pop('info', 'Закладки', item.name + ' добавлен в закладки!');
            }, (err) => { ; this.serverErrorToast(); }, () => {
                this.getMarks();
            });
    }
    saveBook(item: Book) {
        const flagClone = this.checkClonesMarks(item);
        if (flagClone) {
            item.dialogForSave = true;
        }
        else {
            this.saveMark(item);
        }

    }
    unSaveBook(item: Book) {
        item.saveBook = false;
        this.user = this.checkUser();
        if (this.user === null) {
            this.userErrorToast();
            return this.auth.postLogout();
        }
        const obj: Sentence = new Sentence(item, this.user.user);
        this.db
            .postRemoveMark(obj)
            .subscribe((value) => {
                this.toasterService.pop('info', 'Удалён', item.name + ' удалён из закладок');
            }, (err) => { ; this.serverErrorToast(); }, () => {
                this.getMarks();
            });
    }
    checkClonesMarks(item: Book): boolean {
        for (let i = 0; i < this.marks.length; i++) {
            if (this.marks[i]._book.name === item.name) {
                return true;
            }
        }
        return false;
    }
    getMarks() {
        this.user = this.checkUser();
        if (this.user === null) {
            this.userErrorToast();
            return this.auth.postLogout();
        }
        this.db
            .getMarks(this.user.user)
            .subscribe((data) => {
                this.marks = this.storage.parseToObject(data);
            }, (err) => {
                this.toasterService.pop('error', 'Ошибка сервера', 'Не удалось получить данные.');
            }, () => {

            });
    }
    goodSeasonDialog(item: Book) {
        item.dialogForSave = false;
        this.saveMark(item);
    }

    cancelDialog(item: Book) {
        item.dialogForSave = false;
    }
}
