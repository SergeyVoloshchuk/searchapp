"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var auth_service_1 = require("../services/auth.service");
var angular2_toaster_1 = require("angular2-toaster");
var LoginComponent = (function () {
    function LoginComponent(auth, toasterService) {
        this.auth = auth;
        this.formFlag = true;
        this.toasterService = toasterService;
    }
    LoginComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (localStorage.getItem('usr') !== null) {
            this.formFlag = false;
            this.auth.saveLogin(!this.formFlag);
            try {
                var user = JSON.parse(localStorage.getItem('usr'));
                this.profile = user.user;
            }
            catch (e) {
                localStorage.removeItem('usr');
                this.formFlag = true;
                this.auth.saveLogin(!this.formFlag);
                this.auth.postLogout();
            }
        }
        else {
            this.formFlag = true;
            this.auth.saveLogin(!this.formFlag);
        }
        this.auth
            .getIsLogin()
            .subscribe(function (value) {
            _this.formFlag = !value;
        });
    };
    LoginComponent.prototype.incorrectLoginToast = function () {
        this.toasterService.pop('error', 'Ошибка аутентификации', 'Данные введены некорректно!');
    };
    LoginComponent.prototype.serverErrorToast = function () {
        this.toasterService.pop('error', 'Ошибка сервера', 'Не удалось получить данные!');
    };
    LoginComponent.prototype.goodLoginToast = function (userName) {
        this.toasterService.pop('success', 'Успешно', 'Добро пожаловать, ' + userName + '!');
    };
    LoginComponent.prototype.logOutToast = function () {
        this.toasterService.pop('info', 'Выход из аккаунта', 'Всего доброго!');
    };
    LoginComponent.prototype.login = function (form) {
        var _this = this;
        var obj = {
            login: form.value.username,
            pass: form.value.password
        };
        this
            .auth
            .postLogin(obj)
            .subscribe(function (data) {
            _this.result = JSON.parse(data);
        }, function (err) { _this.serverErrorToast(); }, function () {
            if (_this.result) {
                var local = {
                    user: form.value.username,
                    token: _this.result
                };
                localStorage.setItem("usr", JSON.stringify(local));
                _this.profile = "Добро пожаловать, " + form.value.username;
                _this.formFlag = false;
                _this.auth.saveLogin(!_this.formFlag);
                _this.goodLoginToast(form.value.username);
            }
            else {
                _this.formFlag = true;
                _this.auth.saveLogin(!_this.formFlag);
                _this.incorrectLoginToast();
            }
        });
    };
    LoginComponent.prototype.logout = function () {
        localStorage.removeItem('usr');
        this.formFlag = true;
        this.auth.saveLogin(!this.formFlag);
        this.auth.postLogout().subscribe();
        this.logOutToast();
    };
    return LoginComponent;
}());
LoginComponent = __decorate([
    core_1.Component({ selector: 'login', templateUrl: "../app/templates/login.template.html" }),
    __metadata("design:paramtypes", [auth_service_1.AuthService, angular2_toaster_1.ToasterService])
], LoginComponent);
exports.LoginComponent = LoginComponent;
//# sourceMappingURL=login.component.js.map