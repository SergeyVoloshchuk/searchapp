import { Component, Output } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { NgForm } from '@angular/forms';
import { ToasterModule, ToasterService } from 'angular2-toaster';
@Component({ selector: 'login', templateUrl: "./login.template.html" })

export class LoginComponent {
    private toasterService: ToasterService;
    constructor(private auth: AuthService, toasterService: ToasterService) {
        this.toasterService = toasterService;
    }
    ngOnInit() {
        if (localStorage.getItem('usr') !== null) {
            this.formFlag = false;
            this.auth.saveLogin(!this.formFlag);

            try {
                const user = JSON.parse(localStorage.getItem('usr'));
                this.profile = user.user;
            } catch (e) {
                localStorage.removeItem('usr');
                this.formFlag = true;
                this.auth.saveLogin(!this.formFlag);
                this.auth.postLogout();
            }

        } else {
            this.formFlag = true;
            this.auth.saveLogin(!this.formFlag);
        }
        this.auth
            .getIsLogin()
            .subscribe((value: boolean) => {
                this.formFlag = !value;
            });
    }

    formFlag: boolean = true;
    profile: string;
    result: any;
    incorrectLoginToast() {
        this.toasterService.pop('error', 'Ошибка аутентификации', 'Данные введены некорректно!');
    }
    serverErrorToast() {
        this.toasterService.pop('error', 'Ошибка сервера', 'Не удалось получить данные!');
    }
    goodLoginToast(userName: string) {
        this.toasterService.pop('success', 'Успешно', 'Добро пожаловать, ' + userName + '!');
    }
    logOutToast() {
        this.toasterService.pop('info', 'Выход из аккаунта', 'Всего доброго!');
    }
    login(form: NgForm) {
        const obj = {
            login: form.value.username,
            pass: form.value.password
        };
        this
            .auth
            .postLogin(obj)
            .subscribe((data) => {
                this.result = JSON.parse(data);

            }, (err) => { this.serverErrorToast(); }, () => {
                if (this.result) {
                    const local = {
                        user: form.value.username,
                        token: this.result
                    };
                    localStorage.setItem("usr", JSON.stringify(local));
                    this.profile = "Добро пожаловать, " + form.value.username;
                    this.formFlag = false;
                    this.auth.saveLogin(!this.formFlag);
                    this.goodLoginToast(form.value.username);
                } else {
                    this.formFlag = true;
                    this.auth.saveLogin(!this.formFlag);
                    this.incorrectLoginToast();
                }
            });
  
    }

    logout() {
        localStorage.removeItem('usr');
        this.formFlag = true;
        this.auth.saveLogin(!this.formFlag);
        this.auth.postLogout().subscribe();
        this.logOutToast();
    }

}
