import { AuthService } from './../../services/auth.service';
import {
  Component, Input, OnChanges, SimpleChanges, ViewChild
} from '@angular/core';
import { ToasterConfig } from 'angular2-toaster';
@Component({
  selector: 'search-app',
  templateUrl: "./main.template.html",
  styleUrls:['./start.less']
})

export class AppComponent {
  constructor(private auth: AuthService) {
    this.isLogin = false;
  }
  private isLogin: boolean;
  public toasterconfig: ToasterConfig =
  new ToasterConfig({
    newestOnTop: true,
    positionClass: "toast-top-left",
    preventDuplicates: false,
    showCloseButton: true,
    tapToDismiss: true,
    timeout: 2000,
    limit: 5,
    mouseoverTimerStop: true

  });
  ngOnInit() {
    setTimeout(() => {
      this.auth
        .getIsLogin()
        .subscribe((value: boolean) => {
          this.isLogin = value;
        });

    }, 0);
  }

}