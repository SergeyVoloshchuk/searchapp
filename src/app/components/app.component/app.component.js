"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var auth_service_1 = require("./../services/auth.service");
var core_1 = require("@angular/core");
var angular2_toaster_1 = require("angular2-toaster");
var AppComponent = (function () {
    function AppComponent(auth) {
        this.auth = auth;
        this.toasterconfig = new angular2_toaster_1.ToasterConfig({
            newestOnTop: true,
            positionClass: "toast-top-left",
            preventDuplicates: false,
            showCloseButton: true,
            tapToDismiss: true,
            timeout: 2000,
            limit: 5,
            mouseoverTimerStop: true
        });
        this.isLogin = false;
    }
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        setTimeout(function () {
            _this.auth
                .getIsLogin()
                .subscribe(function (value) {
                _this.isLogin = value;
            });
        }, 0);
    };
    return AppComponent;
}());
AppComponent = __decorate([
    core_1.Component({
        selector: 'search-app',
        templateUrl: "../app/templates/main.template.html",
        styleUrls: ['app/css/style.css']
    }),
    __metadata("design:paramtypes", [auth_service_1.AuthService])
], AppComponent);
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map