import { Component } from '@angular/core';

@Component({
	selector: 'not-found-app',
	templateUrl: './404.template.html'
})
export class NotFoundComponent { }