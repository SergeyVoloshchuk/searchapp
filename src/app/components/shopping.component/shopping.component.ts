import { User } from './../../classes/user.class';
import { StorageService } from './../../services/storage.service';
import { Sentence } from './../../classes/sentence.class';
import { DBService } from './../../services/db.service';
import { Component, OnInit } from '@angular/core';
import { ToasterService } from "angular2-toaster";

@Component({
    selector: 'shopping',
    templateUrl: './shopping.template.html',
    styleUrls: ['./shopping.component.less'],
    providers: [DBService, StorageService]
})
export class ShoppingComponent implements OnInit {
    constructor(private db: DBService, private storage: StorageService, toasterService: ToasterService) {
        this.toasterService = toasterService;
    }
    private marks: Array<Sentence> = [];
    private user: User;
    private toasterService: ToasterService;
    private updateFlag: boolean = false;
    ngOnInit() {
        const user = this.storage.getElementSrorage("usr");
        this.user = new User(user.token, user.user);
        this.getMarks();
    }
    getMarks() {
        this.updateFlag = true;
        this.db
            .getMarks(this.user.user)
            .subscribe((data) => {
                this.marks = this.storage.parseToObject(data);
            }, (err) => { this.updateFlag = false; }, () => {
                this.updateFlag = false;
            });
    }
    unSaveBook(item: Sentence) {
        this.updateFlag = true;
        this.db
            .postRemoveMark(item)
            .subscribe((value) => {
                this.toasterService.pop('warning', 'Удалён', item._book.name + ' удалён из закладок');
            }, (err) => { this.updateFlag = false; }, () => {
                this.getMarks();
            });
    }
}