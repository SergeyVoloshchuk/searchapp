"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var db_service_1 = require("../services/db.service");
var angular2_toaster_1 = require("angular2-toaster");
var ng2_bs3_modal_1 = require("ng2-bs3-modal/ng2-bs3-modal");
var _ = require("lodash");
var ListComponent = (function () {
    function ListComponent(db, toasterService) {
        this.db = db;
        this.items = [];
        this.updateItems = [];
        this.saveFlag = false;
        this.procurementList = [];
        this.procListUpdate = [];
        this.messageStatus403 = "Повторите попытку позже";
        this.flagUIStatus403 = true;
        this.toasterService = toasterService;
    }
    ListComponent.prototype.canDeactivate = function () {
        if (this.updateItems.length > 0 && this.saveFlag === true) {
            return confirm("Все несохранённые данные будут утеряны.Вы действительно хотите покинуть страницу?");
        }
        else {
            return true;
        }
    };
    ListComponent.prototype.ngOnInit = function () {
        this.getBooks();
        this.getProcurementList();
    };
    ListComponent.prototype.serverErrorToast = function (err) {
        this.toasterService.pop('error', err.statusText, "Не удалось получить данные");
        this.flagUIStatus403 = false;
    };
    ListComponent.prototype.updateErrorTost = function (err) {
        this.toasterService.pop('error', err.statusText, 'Не удалось обновить!');
    };
    ListComponent.prototype.updateTost = function () {
        this.toasterService.pop('success', 'Успех!', 'Сохранение прошло успешно!');
    };
    ListComponent.prototype.getBooks = function () {
        var _this = this;
        this.db
            .getBooks()
            .subscribe(function (data) {
            _this.items = JSON.parse(data);
        }, function (err) { _this.serverErrorToast(err); }, function () {
        });
    };
    ListComponent.prototype.getProcurementList = function () {
        var _this = this;
        this.db
            .getProcurementList()
            .subscribe(function (data) {
            _this.procurementList = JSON.parse(data);
        }, function (err) { _this.serverErrorToast(err); }, function () {
        });
    };
    ListComponent.prototype.removeBook = function (book) {
        book.remove = true;
        this.updateItems.push(book);
        this.saveItFlag();
    };
    ListComponent.prototype.cancelRemoveBook = function (book) {
        this.updateItems = this.filterItems(book, this.updateItems);
        book.remove = false;
        this.saveItFlag();
    };
    ListComponent.prototype.removeItemProcList = function (book) {
        book.remove = true;
        this.procListUpdate.push(book);
        this.saveItFlag();
    };
    ListComponent.prototype.cancelRemoveProcList = function (book) {
        if (book.status) {
            this.procListUpdate = this.filterItems(book, this.procListUpdate);
            book.remove = false;
        }
        else {
            _.find(this.procListUpdate, function (item) {
                if (item._id == book._id) {
                    book.remove = false;
                    book.status = true;
                }
            });
            this.procListUpdate = this.filterItems(book, this.procListUpdate);
        }
        this.saveItFlag();
    };
    ListComponent.prototype.updateBooksInServer = function () {
        var _this = this;
        this.db
            .removeBooks(this.updateItems)
            .subscribe(function () {
        }, function (err) { _this.updateErrorTost(err); }, function () {
            _this.getBooks();
        });
    };
    ListComponent.prototype.updateProcurementList = function () {
        var _this = this;
        var a = this.db
            .updateBooksInProcurementList(this.procListUpdate)
            .subscribe(function () {
        }, function (err) { _this.updateErrorTost(err); }, function () {
            _this.getProcurementList();
            return true;
        });
        console.log(a);
    };
    ListComponent.prototype.setSaleStatus = function (book) {
        book.status = false;
        this.procListUpdate.push(book);
        this.saveItFlag();
    };
    ListComponent.prototype.saveAndUpdateList = function () {
        this.updateProcurementList();
        this.updateBooksInServer();
        this.updateItems = [];
        this.procListUpdate = [];
        this.saveItFlag();
    };
    ListComponent.prototype.onItemDrop = function (e) {
        this.inProcurementList(e.dragData);
    };
    ListComponent.prototype.inProcurementList = function (item) {
        item.status = true;
        var noRemoveItem = _.clone(item);
        item.remove = true;
        this.updateItems.push(item);
        noRemoveItem.remove = false;
        this.procListUpdate.push(noRemoveItem);
        this.procurementList.push(noRemoveItem);
        this.items = this.filterItems(item, this.items);
        this.saveItFlag();
    };
    ListComponent.prototype.filterItems = function (item, list) {
        return _.filter(list, function (o) {
            return item._id !== o._id;
        });
    };
    ListComponent.prototype.saveItFlag = function () {
        if (this.procListUpdate.length > 0 || this.updateItems.length > 0) {
            this.saveFlag = true;
        }
        else {
            this.saveFlag = false;
        }
    };
    return ListComponent;
}());
__decorate([
    core_1.ViewChild('modalSave'),
    __metadata("design:type", ng2_bs3_modal_1.ModalComponent)
], ListComponent.prototype, "modal", void 0);
ListComponent = __decorate([
    core_1.Component({
        selector: 'list',
        templateUrl: '../app/templates/list.template.html',
        providers: [db_service_1.DBService],
        styleUrls: ['app/css/list.css']
    }),
    __metadata("design:paramtypes", [db_service_1.DBService, angular2_toaster_1.ToasterService])
], ListComponent);
exports.ListComponent = ListComponent;
//# sourceMappingURL=list.component.js.map