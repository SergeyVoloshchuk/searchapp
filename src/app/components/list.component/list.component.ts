import { Subscription } from 'rxjs/Subscription';
import { Sentence } from './../../classes/sentence.class';
import { Component, ViewChild } from '@angular/core';
import { ComponentCanDeactivate } from '../../guards/exit.list.guard';
import { Observable } from "rxjs/Rx";
import { DBService } from '../../services/db.service';
import { ToasterModule, ToasterService } from 'angular2-toaster';
import { ModalComponent } from 'ng2-bs3-modal/ng2-bs3-modal';
import * as _ from 'lodash';
@Component({
    selector: 'list',
    templateUrl: './list.template.html',
    providers: [DBService],
    styleUrls: ['./list.css']
})

export class ListComponent {
    private toasterService: ToasterService;
    items: Array<Sentence> = [];
    private updateItems: Array<Sentence> = [];
    private saveFlag: boolean = false;
    procurementList: Array<Sentence> = [];
    private procListUpdate: Array<Sentence> = [];
    constructor(private db: DBService, toasterService: ToasterService) {
        this.toasterService = toasterService;
    }
    private messageStatus403: string = "Повторите попытку позже";
    private flagUIStatus403: boolean = true;
    private loadFirst: boolean = false;
    private loadSecond: boolean = false;
    private updateFlag: boolean = false;
    @ViewChild('modalSave')
    modal: ModalComponent;
    canDeactivate(): boolean | Observable<boolean> {
        if (this.updateItems.length > 0 && this.saveFlag === true) {
            return confirm("Все несохранённые данные будут утеряны.Вы действительно хотите покинуть страницу?");
        }
        else {
            return true;
        }
    }
    ngOnInit() {
        this.getBooks();
        this.getProcurementList();
    }
    serverErrorToast(err: any) {
        this.toasterService.pop('error', err.statusText, "Не удалось получить данные");
        this.flagUIStatus403 = false;
        this.loadFirst = false;
        this.loadSecond = false;
    }
    updateErrorTost(err: any) {
        this.toasterService.pop('error', err.statusText, 'Не удалось обновить!');
        this.updateFlag = false;
        this.flagUIStatus403 = false;
    }
    updateTost() {
        this.toasterService.pop('success', 'Успех!', 'Сохранение прошло успешно!');
    }
    getBooks() {
        this.loadFirst = true;
        this.db
            .getBooks()
            .subscribe((data) => {
                this.items = JSON.parse(data);

            }, (err) => { this.serverErrorToast(err); }, () => {
                this.loadFirst = false;
            });
    }
    getProcurementList() {
        this.loadSecond = true;
        this.db
            .getProcurementList()
            .subscribe((data) => {
                this.procurementList = JSON.parse(data);

            }, (err) => {
                this.serverErrorToast(err);
            }, () => {
                this.loadSecond = false;
            });
    }
    removeBook(book: Sentence) {
        book.remove = true;
        this.updateItems.push(book);
        this.saveItFlag();
    }
    cancelRemoveBook(book: Sentence) {
        this.updateItems = this.filterItems(book, this.updateItems);
        book.remove = false;
        this.saveItFlag();
    }
    removeItemProcList(book: Sentence) {
        book.remove = true;
        this.procListUpdate.push(book);
        this.saveItFlag();
    }
    cancelRemoveProcList(book: Sentence) {
        if (book.status) {
            this.procListUpdate = this.filterItems(book, this.procListUpdate);
            book.remove = false;

        }
        else {
            _.find(this.procListUpdate, function (item: Sentence) {
                if (item._id == book._id) {
                    book.remove = false;
                    book.status = true;
                }
            });
            this.procListUpdate = this.filterItems(book, this.procListUpdate);
        }
        this.saveItFlag();

    }
    updateBooksInServer() {
        this.updateFlag = true;
        this.db
            .removeBooks(this.updateItems)
            .subscribe(() => {
            }, (err) => { this.updateErrorTost(err); }, () => {
                this.getBooks();
                this.updateFlag = false;
            });
    }
    updateProcurementList() {
        this.updateFlag = true;
        this.db
            .updateBooksInProcurementList(this.procListUpdate)
            .subscribe(() => {
            }, (err) => { this.updateErrorTost(err); }, () => {
                this.getProcurementList();
                this.updateFlag = false;
            });

    }
    setSaleStatus(book: Sentence) {
        book.status = false;
        this.procListUpdate.push(book);
        this.saveItFlag();
    }
    saveAndUpdateList() {
        this.updateProcurementList();
        this.updateBooksInServer();
        this.updateItems = [];
        this.procListUpdate = [];
        this.saveItFlag();
    }
    onItemDrop(e: any) {
        this.inProcurementList(e.dragData);
    }
    inProcurementList(item: Sentence) {
        item.status = true;
        const noRemoveItem: Sentence = _.clone(item);
        item.remove = true;
        this.updateItems.push(item);
        noRemoveItem.remove = false;
        this.procListUpdate.push(noRemoveItem);
        this.procurementList.push(noRemoveItem);
        this.items = this.filterItems(item, this.items);
        this.saveItFlag();
    }
    filterItems(item: Sentence, list: Array<Sentence>): Array<Sentence> {
        return _.filter(list, function (o) {
            return item._id !== o._id
        });
    }

    saveItFlag() {
        if (this.procListUpdate.length > 0 || this.updateItems.length > 0) {
            this.saveFlag = true;
        }
        else {
            this.saveFlag = false;
        }
    }
}
