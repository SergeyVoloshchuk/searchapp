import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from "@angular/router";
import { Observable } from "rxjs/Rx";
import { AuthService } from '../services/auth.service';
import { Injectable } from '@angular/core';
import { ToasterModule, ToasterService } from 'angular2-toaster';
import { StorageService } from './../services/storage.service';
@Injectable()

export class ListGuard implements CanActivate {
	private toasterService: ToasterService;
	private token: any;
	constructor(private authService: AuthService, private router: Router, toasterService: ToasterService, private storage: StorageService) {
		this.toasterService = toasterService;
	}
	badActivate() {
		this.router.navigate(['/'], { queryParams: {} });
		return false;
	}
	canActivate(route: ActivatedRouteSnapshot) {
		const localUser = "usr";
		const localElement = this.storage.getElementSrorage(localUser);
		if (localStorage.getItem(localUser) === null) {
			return this.badActivate();
		}
		if (!localElement) {
			this.storage.localClear();
			this.authService.postLogout();
			this.toasterService.pop('error', 'Ошибка', 'Ошибка чтения данных из локального хранилища!');
			return this.badActivate();
		}
		this.authService.getToken(localElement.token).subscribe((data) => {
			this.token = this.storage.parseToObject(data);
		}, (err) => {
			this.storage.localClear();
			this.authService.postLogout();
			return this.badActivate();
		}, () => {
			if (this.token === null) {
				this.storage.localClear();
				this.authService.postLogout();
				return this.badActivate();
			}
		});
		return true;
	}
}