"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ExitListGuard = (function () {
    function ExitListGuard() {
    }
    ExitListGuard.prototype.canDeactivate = function (component) {
        return component.canDeactivate ? component.canDeactivate() : true;
    };
    return ExitListGuard;
}());
exports.ExitListGuard = ExitListGuard;
//# sourceMappingURL=exit.list.guard.js.map