"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var router_1 = require("@angular/router");
var auth_service_1 = require("../services/auth.service");
var core_1 = require("@angular/core");
var angular2_toaster_1 = require("angular2-toaster");
var ListGuard = (function () {
    function ListGuard(authService, router, toasterService) {
        this.authService = authService;
        this.router = router;
        this.toasterService = toasterService;
    }
    ListGuard.prototype.badActivate = function (state) {
        this.router.navigate(['/'], { queryParams: {} });
        return false;
    };
    ListGuard.prototype.authErrorToast = function () {
        this.toasterService.pop('error', 'Ошибка доступа', 'Необходима авторизация!');
    };
    ListGuard.prototype.tokenErrorToast = function () {
        this.toasterService.pop('error', 'Ошибка', 'Не удалось получить доступ!');
    };
    ListGuard.prototype.userErrorToast = function () {
        this.toasterService.pop('error', 'Нет доступа', 'Нет разрешения!Повторите вход!');
    };
    ListGuard.prototype.errorToast = function () {
        this.toasterService.pop('error', 'Ошибка', 'Ошибка чтения данных из локального хранилища!');
    };
    ListGuard.prototype.canActivate = function (route, state) {
        var _this = this;
        var localUser = "usr";
        if (localStorage.getItem(localUser) === null) {
            this.authErrorToast();
            return this.badActivate(state);
        }
        try {
            var user_1 = JSON.parse(localStorage.getItem(localUser));
            if (user_1 === undefined || user_1.token === undefined) {
                this.tokenErrorToast();
                localStorage.removeItem(localUser);
                this.authService.postLogout();
                return this.badActivate(state);
            }
            this.authService.getToken(user_1.token).subscribe(function (data) {
                if (JSON.parse(data) === user_1.token) {
                    return true;
                }
                else {
                    _this.userErrorToast();
                    localStorage.removeItem(localUser);
                    _this.authService.postLogout();
                    return _this.badActivate(state);
                }
            });
        }
        catch (e) {
            this.errorToast();
            localStorage.removeItem(localUser);
            this.authService.postLogout();
            return this.badActivate(state);
        }
        return true;
    };
    return ListGuard;
}());
ListGuard = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [auth_service_1.AuthService, router_1.Router, angular2_toaster_1.ToasterService])
], ListGuard);
exports.ListGuard = ListGuard;
//# sourceMappingURL=list.guard.js.map