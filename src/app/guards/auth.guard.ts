import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { Observable } from "rxjs/Rx";
import { AuthService } from '../services/auth.service';
import { Injectable } from '@angular/core';
import { ToasterModule, ToasterService } from 'angular2-toaster';
import { StorageService } from './../services/storage.service';
@Injectable()

export class AuthGuard implements CanActivate {
	private toasterService: ToasterService;
	private token: any = null;
	constructor(private authService: AuthService, toasterService: ToasterService, private storage: StorageService) {
		this.toasterService = toasterService;
	}
	canActivate() {
		const localUser = "usr";
		const localElement = this.storage.getElementSrorage(localUser);
		if (localStorage.getItem(localUser) === null) {
			return true;
		}
		if (!localElement) {
			this.storage.localClear();
			this.authService.postLogout();
			this.toasterService.pop('error', 'Ошибка', 'Ошибка чтения данных из локального хранилища!');
			return true;
		}
		this.authService.getToken(localElement.token).subscribe((data) => {
			this.token = this.storage.parseToObject(data);
		}, (err) => {
			this.storage.localClear();
			this.authService.postLogout();
		}, () => {
			if (this.token === null) {
				this.storage.localClear();
				this.authService.postLogout();
			}
		})
		return true;
	}
}