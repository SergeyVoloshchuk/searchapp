import { Injectable } from '@angular/core';


@Injectable()
export class StorageService {

    constructor() { }
    setElementInStorage(key: string, elem: any): boolean {
        try {
            if (!this.checkElement(key) || !this.checkElement(elem)) {
                return false;
            }
            const elemToString = JSON.stringify(elem);
            localStorage.setItem(key, elem);
            return true;
        }
        catch (e) {
            return false;
        }
    }
    getElementSrorage(key: string): any {
        if (!this.checkElement(key) || !this.checkElement(localStorage.getItem(key))) {
            return null;
        }
        const elem = this.parseToObject(localStorage.getItem(key));
        return elem;
    }
    localClear() {
        localStorage.clear();
    }
    checkElement(elem: any): boolean {
        if (elem === undefined || elem === null || (typeof elem === "string" && elem === "")) {
            return false;
        }
        else {
            return true;
        }

    }
    parseToObject(itemStringify: string): any {
        if (this.checkElement(itemStringify)) {
            try {
                const item = JSON.parse(itemStringify);
                return item;
            }
            catch (e) {
                return null;
            }
        }
        else {
            return null;
        }

    }
}