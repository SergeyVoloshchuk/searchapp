import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class AuthService {
	constructor(private http: Http) { }
	private login = new ReplaySubject<boolean>(1);
	postLogin(obj: any) {
		const body = JSON.stringify(obj);
		let headers = new Headers({ 'Content-Type': 'application/json;charset=utf-8' });
		return this.http
			.post('/api/login', body, { headers: headers })
			.map((resp: Response) => resp.json())
			.catch((error: any) => {
				return Observable.throw(error);
			});
	}

	getToken(token: string) {
		return this.http
			.get('/api/token?token=' + token)
			.map((resp: Response) => resp.json())
			.catch((error: any) => {
				return Observable.throw(error);
			});
	}
	postLogout() {
		this.saveLogin(false);
		let headers = new Headers({ 'Content-Type': 'application/json;charset=utf-8' });
		return this.http.post('/api/logout', { headers: headers }).catch((error: any) => {
			return Observable.throw(error);
		});;
	}


	saveLogin(is: boolean) {
		this.login
			.next(is);
	}
	getIsLogin(): Observable<boolean> {
		return this.login;
	}

}