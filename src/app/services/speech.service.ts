import { Injectable, NgZone } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import * as _ from "lodash";

interface IWindow extends Window {
    webkitSpeechRecognition: any;
    SpeechRecognition: any;
}

@Injectable()
export class SpeechRecognitionService {
    speechRecognition: any;

    constructor(private zone: NgZone) {
    }

    record(): Observable<string> {

        return Observable.create((observer: any) => {
            const { webkitSpeechRecognition }: IWindow = <IWindow>window;
            this.speechRecognition = new webkitSpeechRecognition();
            this.speechRecognition.lang = 'ru-RU';
            this.speechRecognition.maxAlternatives = 1;

            this.speechRecognition.onresult = (speech: any) => {
                let term: string = "";
                if (speech.results) {
                    let result: any = speech.results[speech.resultIndex];
                    let transcript: any = result[0].transcript;
                    if (result.isFinal) {
                        if (result[0].confidence < 0.3) {
                        }
                        else {
                            term = _.trim(transcript);
                        }
                    }
                }
                this
                    .zone
                    .run(() => {
                        observer.next(term);
                    });
            };

            this.speechRecognition.onerror = (error: any) => {
                observer.error(error);
            };

            this.speechRecognition.onend = () => {
                observer.complete();
            };
            this.speechRecognition.start();

        });
    }

    DestroySpeechObject() {
        if (this.speechRecognition)
            this.speechRecognition.stop();
    }

}