import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Sentence } from '../classes/sentence.class';
import { Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

@Injectable()
export class DBService {
	constructor(private http: Http) { }
	postBook(obj: Sentence) {
		const body = JSON.stringify(obj);
		let headers = new Headers({ 'Content-Type': 'application/json;charset=utf-8' });
		return this.http
			.post('/api/book', body, { headers: headers })
			.catch((error: any) => {
				return Observable.throw(error);
			});
	}
	getBooks() {
		return this.http
			.get('/api/book')
			.map((resp: Response) => resp.json())
			.catch((error: any) => {
				return Observable.throw(error);
			});
	}
	removeBooks(objs: Sentence[]) {
		const body = JSON.stringify(objs);
		let headers = new Headers({ 'Content-Type': 'application/json;charset=utf-8' });
		return this.http
			.post('/api/update/books', body, { headers: headers })
			.catch((error: any) => {
				return Observable.throw(error);
			});
	}
	updateBooksInProcurementList(objs: Sentence[]) {
		const body = JSON.stringify(objs);
		let headers = new Headers({ 'Content-Type': 'application/json;charset=utf-8' });
		return this.http
			.post('/api/update/proclist', body, { headers: headers })
			.catch((error: any) => {
				return Observable.throw(error);
			});
	}
	getProcurementList() {
		return this.http
			.get('/api/proclist')
			.map((resp: Response) => resp.json())
			.catch((error: any) => {
				return Observable.throw(error);
			});
	}
	postMark(obj: Sentence) {
		const body = JSON.stringify(obj);
		let headers = new Headers({ 'Content-Type': 'application/json;charset=utf-8' });
		return this.http
			.post('/api/mark', body, { headers: headers })
			.catch((error: any) => {
				return Observable.throw(error);
			});
	}
	postRemoveMark(obj: Sentence) {
		const body = JSON.stringify(obj);
		let headers = new Headers({ 'Content-Type': 'application/json;charset=utf-8' });
		return this.http
			.post('/api/removemark', body, { headers: headers })
			.catch((error: any) => {
				return Observable.throw(error);
			});
	}
	getMarks(userName: string) {
		const body = JSON.stringify(userName);
		let headers = new Headers({ 'Content-Type': 'application/json;charset=utf-8' });
		return this.http
			.post('/api/marks', body, { headers: headers })
			.map((resp: Response) => resp.json())
			.catch((error: any) => {
				return Observable.throw(error);
			});
	}

}