import { Pipe, PipeTransform } from "@angular/core";
import * as _ from 'lodash';
import { Book } from '../classes/book.class';

@Pipe({
  name: 'orderBy'
})

export class SortPipe implements PipeTransform {

  transform(arr: Book[]): Book[] {
    const noItemMessage = "Не указан";
    for (let i = 0; i < arr.length; i++) {
      for (let j = arr.length - 1; j > i; j--) {
        let mas1 = _.compact(arr[i].tags);
        let mas2 = _.compact(arr[j].tags);
        _.remove(mas1, function (tag:string) {
          return tag === noItemMessage;
        });
        _.remove(mas2, function (tag:string) {
          return tag === noItemMessage;
        });
        let inter = _.intersection(mas1, mas2);
        if (inter.length > 0) {
          arr[i].cloneBook.push(arr[j]);
          arr.splice(j, 1);
          arr[i].cloneBook.sort((a, b) => {
            return a.price - b.price;
          });
        }
      }
    }
    _.forEach(arr, function (item:Book, index:number) {
      if (item.cloneBook.length > 0 && Number(item.price) > Number(item.cloneBook[0].price)) {
        let elem = item.cloneBook[0];
        item.cloneBook.splice(0, 1);
        let clone = _.clone(item.cloneBook);
        clone.push(item);
        clone.sort((a, b) => {
          return a.price - b.price;
        });
        elem.cloneBook = clone;
        arr[index] = elem;
      }
    });

    return arr;
  }
}