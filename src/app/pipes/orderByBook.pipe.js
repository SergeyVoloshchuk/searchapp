"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var _ = require("lodash");
var SortPipe = (function () {
    function SortPipe() {
    }
    SortPipe.prototype.transform = function (arr) {
        var noItemMessage = "Не указан";
        for (var i = 0; i < arr.length; i++) {
            for (var j = arr.length - 1; j > i; j--) {
                var mas1 = _.compact(arr[i].tags);
                var mas2 = _.compact(arr[j].tags);
                _.remove(mas1, function (tag) {
                    return tag === noItemMessage;
                });
                _.remove(mas2, function (tag) {
                    return tag === noItemMessage;
                });
                var inter = _.intersection(mas1, mas2);
                if (inter.length > 0) {
                    arr[i].cloneBook.push(arr[j]);
                    arr.splice(j, 1);
                    arr[i].cloneBook.sort(function (a, b) {
                        return a.price - b.price;
                    });
                }
            }
        }
        _.forEach(arr, function (item, index) {
            if (item.cloneBook.length > 0 && Number(item.price) > Number(item.cloneBook[0].price)) {
                var elem = item.cloneBook[0];
                item.cloneBook.splice(0, 1);
                var clone = _.clone(item.cloneBook);
                clone.push(item);
                clone.sort(function (a, b) {
                    return a.price - b.price;
                });
                elem.cloneBook = clone;
                arr[index] = elem;
            }
        });
        return arr;
    };
    return SortPipe;
}());
SortPipe = __decorate([
    core_1.Pipe({
        name: 'orderBy'
    })
], SortPipe);
exports.SortPipe = SortPipe;
//# sourceMappingURL=orderByBook.pipe.js.map